from fastapi import FastAPI, UploadFile, File, HTTPException, status
from fastapi.responses import RedirectResponse
from PIL import Image
import io

app = FastAPI()

@app.get('/',include_in_schema=False)
def read_root():
    return RedirectResponse(url='/docs')

@app.post("/img/photos")
def upload(file: UploadFile = File(...)):
    
    png = Image.open(file.file)
    if (file.size/10e5) > 3.5:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="El archivo excede el tamaño")
    
    if file.content_type in ["image/jpeg", "image/jpg", "image/gif", "image/webp"]:
            temp_img = io.BytesIO()
            png.save(temp_img, 'PNG', quality=50)
            png = Image.open(temp_img)
            file.file.close()
            temp_img.close()
            png.close()

    return {'file_sizetemp': png.size, 'file_format': png.format, 'File_size': f'{file.size/10e5} MB'}
    

    